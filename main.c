#include <stdio.h>
#include "dopri5.h"
#include <math.h>

void fcn (unsigned n, double x, double *y, double *f) {
//    f[0] = cos(y[0]+2*y[1]) + 2;
//    f[1] = 2.0 / (x + 2* pow(y[1],2));
    f[0] = sin(5.0*pow(y[0],2)) + x + y[1];
    f[1] = x + y[0] - 3.0 * pow(y[1], 2) + 1;
}

void solout(long nr, double xold, double x, double* y, unsigned n, int* irtrn) {
    for (int i = 0; i < n; ++i) {
        printf("%lf ",y[i]);

    }
    printf("\n");
}

int main() {
    int dimension = 2;
    double x = 1.0;
    double xEnd = 3.0;
    double yValues[] = {1.0, 0.5};
    double rToler[] = {1E-5, 1E-6};
    double aToler[] = {};
    int itoler = 0;
    SolTrait solTrail = NULL;
    int iOut = 1;
    int *fileot = NULL;
    int *icont = NULL;
    int licont = 0;
    double uround = 2.3E-16;
    double safe = 0.9;
    double fac1=0.2,  fac2=10.0;
    double beta = 0.04;
    double hMax = 0.0;
    double h = 0.001;
    double nmax = 0.0;
    double meth = 0.0;
    double nstiff = 0.0;
    double *nrdens = NULL;

    int n = dopri5(
            dimension,
            &fcn,
            x,
            yValues,
            xEnd,
            rToler,
            aToler,
            itoler,
            solout,
            iOut,
            fileot,
            uround,
            safe,
            fac1,
            fac2,
            beta,
            hMax,
            h,
            nmax,
            meth,
            nstiff,
            nrdens,
            icont,
            licont
            );

    for (int i = 0; i < dimension; ++i) {
        printf("%lf\n",yValues[i]);
    }
    printf("%d\n", n);

    return 0;


}




